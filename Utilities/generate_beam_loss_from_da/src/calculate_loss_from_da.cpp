/* Very basic script to calculate the loss from dynamic aperture for a set of double gaussians. 
   define the set of gaussians in dgaussians.csv
   the output will be saved in the directory "output"
*/

#include <stdio.h>      /* printf */
#include <math.h>       /* erfc */
#include <iostream>     /* ifstream */
#include <fstream>      /* cout */
#include <cstdio>
#include <stdlib.h>     /* atof */
#include <sys/stat.h>


double my_erfc(double n) {
    double x;
    x = 1 - erf(n);
    return x; 
}


using namespace std;

int main (int argc, char* argv[])
{

  if (argc != 2) return -1;     // check if the correct number of arguments is given

  char *outdir   = argv[1];     // output directory
  mkdir(outdir, S_IRWXU);       // create output directory
  
  float  da;                    // dynamic aperture
  double a1, a2, sig1, sig2;    // parameters of the double gaussian
  double result;                // result
  double mean, norm;            // calculate the mean loss for each da
  bool calculateLoss = 1;       // logical switch to interrupt losses when they are below 1e-17 for all dists

  
  da = 0.0;
  while (calculateLoss!=0) {

    calculateLoss = 0;
    da = da + 0.1;
    
    char outfilename[30]; // make sure it's big enough
    snprintf(outfilename, sizeof(outfilename), "%s/loss_da_%04.1f.dat", outdir, da);  // create the filename for the output
    
    ofstream myfile;
    myfile.open(outfilename);

    mean  = 0.0;   // initialize the sum
    norm  = 0.0;   // initialize the normalization factor
    
    std::ifstream infile("dgaussians.csv");  
    while (infile >> a1 >> sig1 >> a2 >> sig2)
      {
    
	result = a1*(1 + erf (-da/(sqrt(2.)*sig1)));
	result = result + (1.-a1)*(1. + erf (-da/(sqrt(2.)*sig2)));
	if (result>1.0e-17) {
	  calculateLoss=1;
	}
	myfile << result << "\n";
	mean = mean + result;
	norm = norm + 1.0;
    
      }
    
    myfile.close();

    mean = mean/norm;
    
    printf ("DA %04.1f with mean loss %+.2e \n", da, mean);
    
  }
  
  return 0;
}
